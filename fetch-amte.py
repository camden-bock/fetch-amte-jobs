# a sloppy python script to fetch jobs from amte and export as CSV.
# may add features to only check new jobs, schedule weekly, etc.

import requests
import re

from tabulate import tabulate
from datetime import datetime
import pandas as pd

def extractJobs(url):
    r = requests.get(url)

    query = '(job-heading.*href=")(?P<link>.*)(">)(?P<position>.*)(\s*,\s*)(?P<institution>.*)(<\/a><\/h2>\s*\n\s*.*Posted\sby\s*)(?P<poster>.*)(\s\(.*mailto:)(?P<posteremail>.*)(".*\s*on\s*)(?P<postdate>.*)(.\sReview\sof\sapplicants\sbegins.*"date-display-single">)(?P<review>.*)(<\/span>)'
    query2 = '(Related\sLink.*href=")(?P<relatedLink>(http|https):[^"]*)'

    res = re.finditer(query, r.text)
    table = []
    for match in res:
        position = match.group("position")
        institution = match.group("institution")
        poster_name = match.group("poster")
        poster_email = match.group("posteremail")
        post_date = datetime.strptime(match.group("postdate"), '%b %d, %Y').date()
        review_date = datetime.strptime(match.group("review"), '%B %d, %Y').date()
        amtelink = "https://amte.net" + match.group("link")

        r2 = requests.get(amtelink)
        res2 = re.search(query2, r2.text)
        if(res2 == None):
            relatedLink = ''
        else:
            relatedLink = res2.group("relatedLink")
        table.append([position, institution, poster_name, poster_email, post_date, review_date, amtelink, relatedLink])

    df = pd.DataFrame(table, columns= ['Position', 'Institution', 'Poster', 'PosterEmail', 'PostDate', 'ReviewDate', 'amteLink', 'relatedLink'])
    df = df.sort_values(by='PostDate', ascending=False)

    return df

jobs_df = extractJobs("https://amte.net/resources/joblistings?institution=&job_type=1")
print(tabulate(jobs_df))
jobs_df.to_csv("output_jobs.csv", header=True, index=False, sep='\t')